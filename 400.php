<?php 
    include './inc/pages-header.php'
  ?>
  <!-- header-end -->

  <!-- bradcam_area_start  -->
  <div class="bradcam_area breadcam_bg">
      <div class="container">
          <div class="row">
              <div class="col-xl-12">
                  <div class="bradcam_text">
                      <h3>La Pagina o Recurso no existe</h3>
                      <span><?php echo $_SERVER['REQUEST_URI']; ?> does not exist, sorry.</span>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bradcam_area_end  -->

<?php 
    include './inc/pages-footer.php'
  ?>